<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<meta charset="UTF-8">

<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<!-- CSS  -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!-- <link href="Materialize/css/materialize.css" type="text/css" -->
<!-- 	rel="stylesheet" media="screen,projection" /> -->
<!-- <link href="Materialize/css/style.css" type="text/css" rel="stylesheet" -->
<!-- 	media="screen,projection" /> -->

<!-- CSS読み込み -->
<link rel="stylesheet" href="/baselayout/stylesheet.css">

<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
<!-- <link rel="stylesheet" href="/baselayout/css/all.min.css"> -->

<!--共通ヘッダー-->
<div id="header">
	<nav class="navbar navbar-expand-lg navbar-light">

		<a class="navbar-brand mx-5" style="color: aliceblue" href="Index"><span
			class="material-icons"> local_cafe </span>&emsp;豆&emsp;&emsp;</a>

		<div class="collapse navbar-collapse justify-content-end"
			id="navbarSupportedContent">

			<ul class="navbar-nav">

				<c:if test="${userId == 1}" var="flg" />

				<c:if test="${flg}">
					<li class="nav-item"><a class="nav-link mx-4"
						style="color: aliceblue" href="UserList"><span
							class="material-icons"> person_search </span></a></li>
				</c:if>

				<c:if test="${!flg}">
					<li class="nav-item"><a class="nav-link mx-4"
						style="color: aliceblue" href="UserData?id=${udb.id}"><span
							class="material-icons"> account_circle </span></a></li>
				</c:if>

				<li class="nav-item"><a class="nav-link mx-4"
					style="color: aliceblue" href="Cart"><span
						class="material-icons"> shopping_cart </span></a></li>

				<li class="nav-item"><a class="nav-link mx-4"
					style="color: aliceblue" href="Logout"><span
						class="material-icons"> exit_to_app </span></a></li>

			</ul>
		</div>
	</nav>
</div>