<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userlist</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>ユーザー管理</h1>
		</div>


		<form action="UserList" method="post">

			<input type="hidden" value="${user.id}">

			<div class="form-group row">
				<label for="inputId" class="col-sm-4 col-form-label" align="right">ログインID</label>
				<div class="col-sm-6" align="left">
					<input type="id" style="width: 400px;" class="form-control"
						id="inputId" name="targetLoginId">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputUserName" class="col-sm-4 col-form-label"
					align="right">ユーザー名</label>
				<div class="col-sm-6" align="left">
					<input type="text" style="width: 400px;" class="form-control"
						id="inputUserName" name="targetName">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputBirthday" class="col-sm-4 col-form-label"
					align="right">生年月日</label>
				<div class="col-sm-2" align="left">
					<input type="date" style="width: 170px;" class="form-control"
						name="birthDateFrom">
				</div>
				<span class="form-plaintext">&emsp;～</span>
				<div class="col-sm-2" align="right">
					<input type="date" style="width: 170px;" class="form-control"
						name="birthDateTo">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputAddress" class="col-sm-4 col-form-label"
					align="right">住所</label>
				<div class="col-sm-6" align="left">
					<input type="text" style="width: 400px;" class="form-control"
						id="inputAddress" name="targetAddress">
				</div>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;検索&emsp;&emsp;</button>
				</div>
			</div>


			<hr class="border-bottom border-secondary col-sm-8">

			<div class="py-5">
				<table class="table table-sm col-sm-8 offset-md-2">

					<thead class="thead-light">
						<tr class="d-flex">
							<th class="text-center col-sm-1"></th>
							<th class="text-center col-sm-2">ログインID</th>
							<th class="text-center col-sm-3">ユーザー名</th>
							<th class="text-center col-sm-3">生年月日</th>
							<th class="text-center col-sm-3">住所</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="user" items="${userList}">
							<tr class="d-flex">
								<td class="col-sm-1 text-center"><a
									href="UserData?id=${user.id}"><span class="material-icons">
											search </span></a></td>
								<td class="col-sm-2 text-center">${user.loginId}</td>
								<td class="col-sm-3 text-center">${user.name}</td>
								<td class="col-sm-3 text-center">${user.birthDate}</td>
								<td class="col-sm-3 text-center">${user.address}</td>
							</tr>
						</c:forEach>
					</tbody>

				</table>
			</div>


		</form>


	</div>


</body>


</html>