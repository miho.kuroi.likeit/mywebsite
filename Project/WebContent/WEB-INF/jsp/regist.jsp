<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>regist</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="stylesheet.css">
</head>


<body>
    <div class="container">

        <div class="col-sm-12 text-center" style="padding:50px 50px">
            <h1>新規登録</h1>
        </div>

        <!-- 登録失敗 -->
		<c:if test="${validationMessage != null}">
			<h5 class="text-secondary text-center">${validationMessage}</h5>
			<br>
		</c:if>

        <form action="RegistConfirm" method="post">

            <div class="form-group row">
                <label for="inputId" class="col-sm-5 col-form-label" align="right">ログインID</label>
                <div class="col-sm-5" align="left">
                    <input type="id" style="width:250px;" class="form-control" id="inputId" name="login_id" value="${udb.loginId}">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-5 col-form-label" align="right">パスワード</label>
                <div class="col-sm-5" align="left">
                    <input type="password" style="width:250px;" class="form-control" id="inputPassword" name="password">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-5 col-form-label" align="right">パスワード(確認)</label>
                <div class="col-sm-5" align="left">
                    <input type="password" style="width:250px;" class="form-control" id="inputPassword" name="confirm_password">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputUserName" class="col-sm-5 col-form-label" align="right">ユーザー名</label>
                <div class="col-sm-5" align="left">
                    <input type="text" style="width:250px;" class="form-control" id="inputUserName" name="user_name" value="${udb.name}">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputBirthday" class="col-sm-5 col-form-label" align="right">生年月日</label>
                <div class="col-sm-5" align="left">
                    <input type="date" style="width:250px;" class="form-control" id="inputBirthday" name="birth_date" value="${udb.birthDate}">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputAddress" class="col-sm-5 col-form-label" align="right">住所</label>
                <div class="col-sm-5" align="left">
                    <input type="text" style="width:250px;" class="form-control" id="inputAddress" name="user_address" value="${udb.address}">
                </div>
            </div>

            <div class="form-group row" align="center" style="padding:50px 0px">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;登録&emsp;&emsp;</button>
                </div>
            </div>

        </form>


        <div class="form-group row" align="right" style="padding-bottom:50px">
            <div class="col-sm-4">
                <a type="submit" class="text-secondary border-bottom border-secondary" href="Login">戻る</a>
            </div>
        </div>


    </div>
</body></html>