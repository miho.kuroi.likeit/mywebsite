<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>itemSearchResult</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">

</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>検索結果</h1>
			<h5>${itemCount}件</h5>
		</div>

		<c:if test="${userId == 1}">
			<div class="form-group row" align="right"
				style="padding-bottom: 50px">
				<div class="col-sm-12">
					<a type="submit" class="btn btn-outline-secondary"
						href="ItemArrival">&emsp;商品登録&emsp;</a>
				</div>
			</div>
		</c:if>

		<div class="section" style="padding-bottom: 50px">
			<!--   商品情報   -->
			<div class="col-sm-12">
				<div class="row">
					<c:forEach var="item" items="${itemList}" varStatus="status">

						<div class="col-sm-4">
							<div class="card-image" align="center">
								<a href="Item?item_id=${item.id}&page_num=${pageNum}"><img
									src="img/${item.fileName}"></a>
							</div>
							<div class="card-body">
								<span class="card-title">${item.name}</span>
								<p class="card-text">${item.origin}</p>
								<p class="card-text">${item.formatPrice}円</p>
							</div>
						</div>

					</c:forEach>
				</div>
			</div>
		</div>

		<div>
			<ul class="pagination justify-content-center">

				<!-- ItemSearchResult?search_word=${searchWord}&origin=${idb.origin}&price_check=${priceCheck}&price=${idb.price}&taste_check=${tasteCheck}&taste_a=${idb.tasteA}&taste_b=${idb.tasteB} -->

				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a><i class="fas fa-chevron-left"></i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a
							href="ItemSearchResult?search_word=${searchWord}&origin=${idb.origin}&price_check=${priceCheck}&price=${idb.price}&taste_check=${tasteCheck}&taste_a=${idb.tasteA}&taste_b=${idb.tasteB}&page_num=${pageNum - 1}"><span
								class="material-icons"> chevron_left </span></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}"
					end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1"
					varStatus="status">
					<li
						<c:if test="${pageNum == status.index }"> class="active" </c:if>><a
						href="ItemSearchResult?search_word=${searchWord}&origin=${idb.origin}&price_check=${priceCheck}&price=${idb.price}&taste_check=${tasteCheck}&taste_a=${idb.tasteA}&taste_b=${idb.tasteB}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
					<c:when test="${pageNum == pageMax || pageMax == 0}">
						<li class="disabled"><a><i class="fas fa-chevron-right"></i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a
							href="ItemSearchResult?search_word=${searchWord}&origin=${idb.origin}&price_check=${priceCheck}&price=${idb.price}&taste_check=${tasteCheck}&taste_a=${idb.tasteA}&taste_b=${idb.tasteB}&page_num=${pageNum + 1}"><span
								class="material-icons"> chevron_right </span></a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>

		<div class="form-group row" align="right" style="padding: 50px 0">
			<div class="col-sm-1">
				<a type="submit"
					class="text-secondary border-bottom border-secondary" href="Index">戻る</a>
			</div>
		</div>

	</div>
</body>

</html>