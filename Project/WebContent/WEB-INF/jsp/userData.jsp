<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userData</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>マイページ</h1>
		</div>

		<!-- 登録失敗 -->
		<c:if test="${validationMessage != null}">
			<h5 class="text-secondary text-center">${validationMessage}</h5>
			<br>
		</c:if>


		<form action="UserDataUpdateConfirm" method="post" id="update">

			<input type="hidden" value="${udb.id}" name="id">

			<div class="form-group row">

				<label for="inputId" class="col-sm-2 col-form-label offset-1"
					align="center">ログインID</label>
				<div class="col-sm-2" align="left">
					<input type="id" readonly="" class="form-control-plaintext"
						id="staticId" name="login_id" value="${udb.loginId}">
				</div>

				<label for="inputUserName" class="col-sm-2 col-form-label offset-1"
					align="center">ユーザー名</label>
				<div class="col-sm-2" align="left">
					<input type="text" style="width: 200px;" class="form-control"
						id="inputUserName" name="user_name" value="${udb.name}">
				</div>

			</div>

			<div class="form-group row">

				<label for="inputPassword" class="col-sm-2 col-form-label offset-1"
					align="center">パスワード</label>
				<div class="col-sm-2" align="left">
					<input type="password" style="width: 200px;" class="form-control"
						name="password" id="inputPassword">
				</div>

				<label for="inputBirthday" class="col-sm-2 col-form-label offset-1"
					align="center">生年月日</label>
				<div class="col-sm-2" align="left">
					<input type="date" style="width: 200px;" class="form-control"
						name="birth_date" value="${udb.birthDate}">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputPassword" class="col-sm-2 col-form-label offset-1"
					align="center">パスワード(確認)</label>
				<div class="col-sm-2" align="left">
					<input type="password" style="width: 200px;" class="form-control"
						name="confirm_password" id="inputPassword">
				</div>
				<label for="inputAddress" class="col-sm-2 col-form-label offset-1"
					align="center">住所</label>
				<div class="col-sm-2" align="left">
					<input type="text" style="width: 200px;" class="form-control"
						name="user_address" id="inputAddress" value="${udb.address}">
				</div>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;更新&emsp;&emsp;</button>
				</div>
			</div>

		</form>


		<hr class="border-bottom border-secondary col-sm-8">

		<div class="py-5">
			<table class="table table-sm col-sm-8 offset-md-2">

				<thead class="thead-light">
					<tr class="d-flex">
						<th class="text-center col-sm-2"></th>
						<th class="text-center col-sm-4">日時</th>
						<th class="text-center col-sm-3">配送方法</th>
						<th class="text-center col-sm-3">金額</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var="bdb" items="${bdb}">
						<c:if test="${!empty bdb.getFormatDate()}">
							<tr class="d-flex">
								<td class="col-sm-2 text-center"><a
									href="UserBuyHistoryDetail?id=${bdb.getId()}"><span
										class="material-icons"> receipt_long </span></a></td>
								<td class="col-sm-4 text-center">${bdb.getFormatDate()}</td>
								<td class="col-sm-3 text-center">${bdb.getDeliveryMethodName()}配送</td>
								<td class="col-sm-3 text-center">${bdb.getFormatTotalPrice()}円</td>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>

			<c:if test="${bdb.size()==0 }">
				<p class="col-sm-12" align="center">購入履歴がありません。</p>
			</c:if>
		</div>

		<form action="UserDataDeleteConfirm" method="get" id="delete">

			<input type="hidden" value="${udb.id}" name="id">

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;退会&emsp;&emsp;</button>
				</div>
			</div>
		</form>


	</div>
</body>
</html>