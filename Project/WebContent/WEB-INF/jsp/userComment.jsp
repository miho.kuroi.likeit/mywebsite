<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userComment</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>


<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- 本体 ー-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 0px 50px 50px">
			<h1>コメント</h1>
		</div>

		<form action="UserCommentDeleteResult" method="post" id="delete">
			<input type="hidden" value="${id}" name="id">
			<div class="form-group row" align="right">
				<div class="col-sm-9">
					<button type="submit" class="btn btn-outline-secondary">
						<span class="material-icons"> delete </span>
					</button>
				</div>
			</div>
		</form>

		<form action="UserCommentResult" method="post" id="comment">
			<input type="hidden" value="${id}" name="id">
			<div class="form-group" align="center">
				<div class="col-sm-6">
					<textarea class="form-control" id="FormControlTextarea1" rows="3"
						name="comment">${comment}</textarea>
				</div>
			</div>
			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;登録&emsp;&emsp;</button>
				</div>
			</div>
		</form>


		<div class="form-group row" align="right" style="padding-bottom: 50px">
			<div class="col-sm-3">
				<a type="submit" href="${referer}"
					class="text-secondary border-bottom border-secondary">戻る</a>
			</div>
		</div>


	</div>
</body>
</html>