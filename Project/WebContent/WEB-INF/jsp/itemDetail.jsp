<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>itemDetail</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>商品詳細</h1>
		</div>

		<c:if test="${userId == 1}">
			<div class="form-group row" align="right"
				style="padding-bottom: 50px">
				<div class="col-sm-11">
					<a type="submit" class="btn btn-outline-secondary"
						href="ItemUpdate?item_id=${idb.id}&page_num=${pageNum}">&emsp;更新&emsp;</a>
					<a type="submit" class="btn btn-outline-secondary"
						href="ItemDelete?item_id=${idb.id}&page_num=${pageNum}">&emsp;削除&emsp;</a>
				</div>
			</div>
		</c:if>

		<form action="CartAdd" method="post">

			<input type="hidden" value="${idb.id}" name="item_id">

			<div class="form-group row">

				<div class="col-sm-5" align="right">
					<img src="img/${idb.fileName}" class="img-fluid"
						alt="Responsive image">
				</div>

				<div class="col-sm-5 offset-1" align="left">
					<h2>${idb.name}</h2>
					<h4>${idb.formatPrice}円</h4>
					<h4>${idb.origin}</h4>
					<fieldset disabled>
						<h4>
							&emsp;酸味&emsp; <input type="range" style="width: 150px;"
								class="custom-range" min="1" max="5" step="1" id="inputTaste1"
								value="${idb.tasteA}" readonly> 苦味
						</h4>
						<h4>
							スッキリ <input type="range" style="width: 150px;"
								class="custom-range" min="1" max="5" step="1" id="inputTaste2"
								value="${idb.tasteB}" readonly> コク
						</h4>
					</fieldset>
					<p>${idb.detail}</p>

				</div>
			</div>


			<div class="form-group row" align="center" style="padding: 50px 0px">

				<div class="col-sm-12">
					<select class="custom" id="inlineFormCustomSelect" name="amount"
						style="margin-bottom: 20px">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select>個
				</div>

				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;カートに入れる&emsp;&emsp;</button>
				</div>
			</div>

		</form>

		<div class="row center">
			<h5 class="col-sm-5 offset-1" style="padding: 20px">
				<span class="material-icons"> comment </span> 購入者コメント
			</h5>
		</div>

		<c:forEach var="bddb" items="${bddb}">
			<c:choose>
				<c:when test="${!empty bddb.getComment()}">
					<div class="card-deck col-sm-12" style="padding-bottom: 50px">
						<div class="card col-sm-4">
							<div class="card-body">
								<p class="card-text">${bddb.getComment()}</p>
							</div>
						</div>
					</div>
				</c:when>
			</c:choose>
		</c:forEach>
		<c:if test="${bddb.size()==0 }">
			<p class="col-sm-12" align="center">コメントがありません。</p>
		</c:if>


		<div class="form-group row" align="right" style="padding: 50px 0px">
			<div class="col-sm-2" align="right">
				<a type="submit"
					class="text-secondary border-bottom border-secondary"
					href="${referer}">戻る</a>
				<!-- <a type="submit"
						class="text-secondary border-bottom border-secondary"
						href="ItemSearchResult?search_word=${searchWord}&origin=${searchIdb.getOrigin()}&price_check=${priceCheck}&price=${searchIdb.getPrice()}&taste_check=${tasteCheck}&taste_a=${searchIdb.getTasteA()}&taste_b=${searchIdb.getTasteB()}">戻る</a>
						-->
			</div>
		</div>
</body>
</html>