<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>buyResult</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>購入完了</h1>
		</div>

		<div class="col-sm-12" align="center">
			<p>ありがとうございました！</p>
		</div>

		<div class="py-5">
			<h4 class="text-center" style="padding: 50px 50px">購入明細</h4>
			<table class="table table-sm col-sm-8 offset-md-2">

				<thead class="thead-light">
					<tr class="d-flex">
						<th class="text-center col-sm-4">日時</th>
						<th class="text-center col-sm-4">配送方法</th>
						<th class="text-center col-sm-4">金額</th>
					</tr>
				</thead>

				<tbody>
					<tr class="d-flex">
						<td class="col-sm-4 text-center">${resultBDB.formatDate}</td>
						<td class="col-sm-4 text-center">${resultBDB.deliveryMethodName}配送</td>
						<td class="col-sm-4 text-center">${resultBDB.formatTotalPrice}円</td>
					</tr>
				</tbody>

			</table>

			<table class="table table-sm col-sm-8 offset-md-2">

				<thead class="thead-light">
					<tr class="d-flex">
						<th class="text-center col-sm-3">商品名</th>
						<th class="text-center col-sm-3">単価</th>
						<th class="text-center col-sm-3">個数</th>
						<th class="text-center col-sm-3">小計</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var="buyIDB" items="${buyIDBList}">
						<tr class="d-flex">
							<td class="col-sm-3 text-center">${buyIDB.name}</td>
							<td class="col-sm-3 text-center">${buyIDB.formatPrice}円</td>
							<td class="col-sm-3 text-center">${buyIDB.amount}</td>
							<td class="col-sm-3 text-center">${buyIDB.formatTotalPrice}円</td>
						</tr>
					</c:forEach>
				</tbody>

				<tbody>
					<tr class="d-flex">
						<td class="col-sm-3 text-center"></td>
						<td class="col-sm-3 text-center"></td>
						<td class="col-sm-3 text-center">${resultBDB.deliveryMethodName}配送</td>
						<td class="col-sm-3 text-center">${resultBDB.deliveryMethodPrice}円</td>
					</tr>
				</tbody>

			</table>
		</div>


		<div align="center" style="padding: 50px 50px">
			<a type="button" href="Index"
				class="col-sm-6 btn btn-secondary btn-lg">トップページに戻る</a>
		</div>


	</div>


</body>
</html>