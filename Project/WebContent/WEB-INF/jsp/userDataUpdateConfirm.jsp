<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userDataUpdateConfirm</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>


<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- 本体 ー-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>更新内容確認</h1>
		</div>


		<form action="UserDataUpdateResult" method="post">

			<input type="hidden" value="${udb.id}" name="id">

			<div class="form-group row">
				<label for="inputId" class="col-sm-5 col-form-label" align="right">ログインID</label>
				<div class="col-sm-5" align="left">
					<input type="id" readonly="" class="form-control-plaintext"
						id="staticId" name="login_id" value="${udb.loginId}">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputPassword" class="col-sm-5 col-form-label"
					align="right">パスワード</label>
				<div class="col-sm-5" align="left">
					<input type="password" readonly="" class="form-control-plaintext"
						id="staticPassword" name="password" value="${udb.password}">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputUserName" class="col-sm-5 col-form-label"
					align="right">ユーザー名</label>
				<div class="col-sm-5" align="left">
					<input type="text" readonly="" class="form-control-plaintext"
						id="staticUserName" name="user_name" value="${udb.name}">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputBirthday" class="col-sm-5 col-form-label"
					align="right">生年月日</label>
				<div class="col-sm-5" align="left">
					<input type="date" readonly="" class="form-control-plaintext"
						id="staticBirthday" name="birth_date" value="${udb.birthDate}">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputAddress" class="col-sm-5 col-form-label"
					align="right">住所</label>
				<div class="col-sm-5" align="left">
					<input type="text" readonly="" class="form-control-plaintext"
						id="staticAddress" name="user_address" value="${udb.address}">
				</div>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<p>上記の内容で更新してよろしいでしょうか</p>
					<button type="submit" class="btn btn-outline-secondary"
						name="confirm_button" value="cancel">&emsp;&emsp;修正&emsp;&emsp;</button>
					<button type="submit" class="btn btn-outline-secondary offset-md-2"
						name="confirm_button" value="update">&emsp;&emsp;更新&emsp;&emsp;</button>
				</div>
			</div>

		</form>

	</div>
</body>
</html>