<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>logout</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="stylesheet.css">
</head>


<body>
    <div class="container">

        <div class="col-sm-12 text-center" style="padding:50px 50px"><h1>ログアウトしました</h1></div>

        <div align="center" style="padding-bottom: 50px">
        <a type="button" class="col-sm-6 btn btn-secondary btn-lg" href="Login">ログイン画面に戻る</a>
        </div>
    </div>

</body>

</html>
