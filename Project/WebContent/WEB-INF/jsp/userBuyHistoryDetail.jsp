<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userBuyHistoryDetail</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>


</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">



		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>購入履歴詳細</h1>
		</div>

		<div class="py-5">
			<table class="table table-sm col-sm-8 offset-md-2">

				<thead class="thead-light">
					<tr class="d-flex">
						<th class="text-center col-sm-4">日時</th>
						<th class="text-center col-sm-4">配送方法</th>
						<th class="text-center col-sm-4">金額</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var="bdb" items="${bdb}">
						<tr class="d-flex">
							<td class="col-sm-4 text-center">${bdb.getFormatDate()}</td>
							<td class="col-sm-4 text-center">${bdb.getDeliveryMethodName()}配送</td>
							<td class="col-sm-4 text-center">${bdb.getFormatTotalPrice()}円</td>
						</tr>
					</c:forEach>
				</tbody>

			</table>
		</div>

		<hr class="border-bottom border-secondary col-sm-8">


		<div class="py-5">
			<table class="table table-sm col-sm-8 offset-md-2">

				<thead class="thead-light">
					<tr class="d-flex">
						<th class="text-center col-sm-7">商品名</th>
						<th class="text-center col-sm-3">単価</th>
						<th class="text-center col-sm-1">個数</th>
						<th class="text-center col-sm-1"></th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var="idb" items="${idb}">
						<tr class="d-flex">
							<td class="col-sm-7 text-center">${idb.getName()}</td>
							<td class="col-sm-3 text-center">${idb.getFormatPrice()}円</td>
							<td class="col-sm-1 text-center">${idb.getAmount()}</td>
							<td class="col-sm-1 text-center"><a
								href="UserComment?id=${idb.getDetailId()}"> <span
									class="material-icons"> comment </span>
							</a></td>
						</tr>
					</c:forEach>

					<c:forEach var="bdb" items="${bdb}">
						<tr class="d-flex">
							<td class="col-sm-7 text-center">${bdb.getDeliveryMethodName()}配送</td>
							<td class="col-sm-3 text-center">${bdb.getDeliveryMethodPrice()}円</td>
							<td class="col-sm-1 text-center"></td>
							<td class="col-sm-1 text-center"></td>
						</tr>
					</c:forEach>
				</tbody>

			</table>
		</div>

		<div class="form-group row" align="right" style="padding: 50px 50px">
			<div class="col-sm-1">
				<a type="submit" href="UserData?id=${udb.id}"
					class="text-secondary border-bottom border-secondary">戻る</a>
			</div>
		</div>
	</div>


</body>
</html>