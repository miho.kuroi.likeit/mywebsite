<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userCommentDeleteResult</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>


<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>コメント削除完了</h1>
		</div>

		<div class="form-group" align="center">
			<div class="col-sm-6">
				<textarea class="form-control" readonly=""
					　id="FormControlTextarea1" rows="3">${comment}</textarea>
			</div>
		</div>

		<div class="form-group row" align="center" style="padding: 50px 50px">
			<div class="col-sm-12">
				<p>上記の内容を削除しました！</p>
				<a type="button" href="UserData" class="col-sm-6 btn btn-secondary btn-lg">マイページへ</a>
			</div>
		</div>

	</div>
</body>
</html>