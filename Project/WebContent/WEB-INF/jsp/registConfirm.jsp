<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>registconfirm</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="stylesheet.css">
</head>


<body>
     <div class="container">

         <div class="col-sm-12 text-center" style="padding:50px 50px"><h1>入力内容確認</h1></div>


         <form action="RegistResult" method="post">

             <div class="form-group row">
                 <label for="inputId" class="col-sm-5 col-form-label" align="right">ログインID</label>
                 <div class="col-sm-5" align="left">
                     <input type="id" name="login_id" readonly="" class="form-control-plaintext" id="staticId" value="${udb.loginId}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputPassword" class="col-sm-5 col-form-label" align="right">パスワード</label>
                 <div class="col-sm-5" align="left">
                     <input type="password" name="password" readonly="" class="form-control-plaintext" id="staticPassword" value="${udb.password}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputUserName" class="col-sm-5 col-form-label" align="right">ユーザー名</label>
                 <div class="col-sm-5" align="left">
                     <input name="user_name" type="text" readonly="" class="form-control-plaintext" id="staticUserName" value="${udb.name}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputBirthday" class="col-sm-5 col-form-label" align="right">生年月日</label>
                 <div class="col-sm-5" align="left">
                     <input name="birth_date" type="date" readonly="" class="form-control-plaintext" id="staticBirthday" value="${udb.birthDate}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputAddress" class="col-sm-5 col-form-label" align="right">住所</label>
                 <div class="col-sm-5" align="left">
                     <input name="user_address" type="text" readonly="" class="form-control-plaintext" id="staticAddress" value="${udb.address}">
                 </div>
             </div>

             <div class="form-group row" align="center" style="padding:50px 50px">
                 <div class="col-sm-12">
                     <p>上記の内容で登録します</p>
                     <button type="submit" class="btn btn-outline-secondary" name="confirm_button" value="cancel">&emsp;&emsp;修正&emsp;&emsp;</button>
                     <button type="submit" class="btn btn-outline-secondary offset-md-2" name="confirm_button" value="regist">&emsp;&emsp;登録&emsp;&emsp;</button>
                 </div>
             </div>

         </form>

    </div>
</body>
</html>