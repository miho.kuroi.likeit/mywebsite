<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>registresult</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="stylesheet.css">
</head>


<body>
     <div class="container">

         <div class="col-sm-12 text-center" style="padding:50px 50px"><h1>登録完了</h1></div>

             <div class="form-group row">
                 <label for="inputId" class="col-sm-5 col-form-label" align="right">ログインID</label>
                 <div class="col-sm-5" align="left">
                     <input type="id" readonly="" class="form-control-plaintext" id="staticId" value="${udb.loginId}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputPassword" class="col-sm-5 col-form-label" align="right">パスワード</label>
                 <div class="col-sm-5" align="left">
                     <input type="password" readonly="" class="form-control-plaintext" id="staticPassword" value="${udb.password}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputUserName" class="col-sm-5 col-form-label" align="right">ユーザー名</label>
                 <div class="col-sm-5" align="left">
                     <input type="text" readonly="" class="form-control-plaintext" id="staticUserName" value="${udb.name}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputBirthday" class="col-sm-5 col-form-label" align="right">生年月日</label>
                 <div class="col-sm-5" align="left">
                     <input type="date" readonly="" class="form-control-plaintext" id="staticBirthday" value="${udb.birthDate}">
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputAddress" class="col-sm-5 col-form-label" align="right">住所</label>
                 <div class="col-sm-5" align="left">
                     <input type="text" readonly="" class="form-control-plaintext" id="staticAddress" value="${udb.address}">
                 </div>
             </div>

             <div class="form-group row" align="center" style="padding:50px 50px">
                 <div class="col-sm-12">
                     <p>上記の内容で登録しました！</p>
                     <a type="button" class="col-sm-6 btn btn-secondary btn-lg" href="Login">ログイン画面へ</a>
                 </div>
             </div>

    </div>
</body>
</html>