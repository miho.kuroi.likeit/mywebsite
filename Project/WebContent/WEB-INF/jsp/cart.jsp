<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>cart</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<script src="https://kit.fontawesome.com/6c4d6d73a4.js"
	crossorigin="anonymous"></script>
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>カート</h1>
		</div>

		<!-- 買い物メッセージ -->
		<c:if test="${cartActionMessage != null}">
			<h5 class="text-secondary text-center">${cartActionMessage}</h5>
			<br>
		</c:if>

		<form action="CartDelete" method="post">

			<!-- <input type="hidden" value="${idb.id}" name="id">  -->

			<div class="form-group row" align="right"
				style="padding-bottom: 50px">
				<div class="col-sm-11">
					<button type="submit" class="btn btn-outline-secondary">&emsp;削除&emsp;</button>
				</div>
			</div>


			<div class="section">
				<!--   商品情報   -->
				<div class="col-sm-12">
					<div class="row">
						<c:forEach var="item" items="${cart}" varStatus="status">
							<div class="card col-sm-4">
								<div align="center">
									<img src="img/${item.fileName}" />
								</div>
								<div class="card-body">
									<div class="form-check text-right">
										<input class="form-check-input" type="checkbox"
											id="${status.index}" name="delete_item_id_list"
											value="${item.id}"> <label class="form-check-label"
											for="${status.index}"> <span class="material-icons">
												delete </span>
										</label>
									</div>
									<h5 class="card-title">${item.name}</h5>
									<p class="card-text">${item.origin}</p>
									<p class="card-text">個数：${item.amount}</p>
								</div>
							</div>

						</c:forEach>
					</div>
				</div>
			</div>

		</form>


		<div class="form-group row" align="center" style="padding: 50px 50px">
			<div class="col-sm-12">
				<a type="submit" class="btn btn-outline-secondary" href="Buy">&emsp;&emsp;購入手続きに進む&emsp;&emsp;</a>
			</div>
		</div>

		<div class="form-group row" align="right" style="padding-bottom: 50px">
			<div class="col-sm-1">
				<a type="submit" href="${referer}"
					class="text-secondary border-bottom border-secondary">戻る</a>
			</div>
		</div>



	</div>

</body>

</html>