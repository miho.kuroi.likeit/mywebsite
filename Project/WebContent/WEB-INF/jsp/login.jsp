<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="stylesheet.css">
</head>


<body>
	<div class="container">


		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>ログイン画面</h1>
		</div>

		<!-- ログイン失敗 -->
		<c:if test="${loginErrorMessage != null}">
			<h5 class="text-secondary text-center">${loginErrorMessage}</h5>
			<br>
		</c:if>

		<form action="LoginResult" method="post">

			<div class="form-group row">
				<label for="inputLoginId" class="col-sm-5 col-form-label"
					align="right">ログインID</label>
				<div class="col-sm-5" align="left">
					<input type="id" style="width: 250px;" class="form-control"
						id="inputLoginId" name="login_id">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputPassword" class="col-sm-5 col-form-label"
					align="right">パスワード</label>
				<div class="col-sm-5" align="left">
					<input type="password" style="width: 250px;" class="form-control"
						id="inputPassword" name="password">
				</div>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;ログイン&emsp;</button>
				</div>
			</div>

			<div class="form-group row" align="right"
				style="padding-bottom: 20px">
				<div class="col-sm-8">
					<a class="text-secondary border-bottom border-secondary"
						href="Regist">新規登録</a>
				</div>
			</div>

		</form>
	</div>
</body>

</html>
