<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>buy</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<!-- <script src="https://kit.fontawesome.com/6c4d6d73a4.js" -->
<!-- 	crossorigin="anonymous"></script> -->
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>カートアイテム</h1>
		</div>


		<form action="BuyConfirm" method="post">

			<div class="py-5">

				<table class="table table-sm col-sm-8 offset-md-2">

					<thead class="thead-light">
						<tr class="d-flex">
							<th class="text-center col-sm-3">商品名</th>
							<th class="text-center col-sm-3">単価</th>
							<th class="text-center col-sm-3">個数</th>
							<th class="text-center col-sm-3">小計</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="cartInItem" items="${cart}">
							<tr class="d-flex">
								<td class="col-sm-3 text-center">${cartInItem.name}</td>
								<td class="col-sm-3 text-center">${cartInItem.formatPrice}円</td>
								<td class="col-sm-3 text-center">${cartInItem.amount}</td>
								<td class="col-sm-3 text-center">${cartInItem.formatTotalPrice}円</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

			<div class="table table-sm col-sm-10 d-flex justify-content-end">
				<!-- <select class="custom-select col-sm-3">
					<option value="1">通常配送:0円</option>
					<option value="2">お急ぎ配送:500円</option>
					<option value="3">日時指定配送:300円</option>
				</select>
				-->

				<select class="custom-select col-sm-3" name="delivery_method_id">
					<c:forEach var="dmdb" items="${dmdbList}">
						<option value="${dmdb.id}">${dmdb.name}:${dmdb.price}円</option>
					</c:forEach>
				</select>


			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;購入確認&emsp;&emsp;</button>
				</div>
			</div>

		</form>

		<div class="form-group row" align="right" style="padding-bottom: 50px">
			<div class="col-sm-2">
				<a type="submit" href="Cart"
					class="text-secondary border-bottom border-secondary">戻る</a>
			</div>
		</div>


	</div>


</body>
</html>