<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>itemUpdate</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>商品情報更新</h1>
		</div>

		<form action="ItemUpdate" method="post">

			<input type="hidden" value="${idb.id}" name="item_id">

			<div class="form-group row">

				<div class="col-sm-5" align="right">
					<img src="img/${idb.fileName}" class="img-fluid"
						alt="Responsive image">
					<div class="form-group row" style="padding-top: 20px">
						<label for="inputItemPic" class="col-sm-4 col-form-label"
							align="right">画像</label>
						<div class="" align="left">
							<input type="file" class="form-control-file" id="inputItemPic"
								name="file_name">
							<!-- input type="file"はvalue値を設定できない -->
						</div>
					</div>
				</div>

				<div class="col-sm-6" align="left">
					<div class="form-group row">
						<label for="inputItemName" class="col-sm-3 col-form-label"
							align="right">商品名</label>
						<div class="col-sm-5" align="left">
							<input type="text" style="width: 400px;" class="form-control"
								id="inputItemName" name="name" value="${idb.name}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputItemPrice" class="col-sm-3 col-form-label"
							align="right">単価</label>
						<div class="col-sm-5" align="left">
							<input type="text" style="width: 400px;" class="form-control"
								id="inputItemPrice" name="price" value="${idb.price}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputItemOrigin" class="col-sm-3 col-form-label"
							align="right">原産地</label>
						<div class="col-sm-5" align="left">
							<input type="text" style="width: 400px;" class="form-control"
								id="inputItemOrigin" name="origin" value="${idb.origin}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputItemTaste1" class="col-sm-3 col-form-label"
							align="right">酸味⇔苦味</label>
						<div class="col-sm-5" align="left">
							<input type="range" style="width: 400px;" class="custom-range"
								min="1" max="5" step="1" id="inputItemTaste1" name="taste_a"
								value="${idb.tasteA}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputItemTaste2" class="col-sm-3 col-form-label"
							align="right">スッキリ⇔コク</label>
						<div class="col-sm-5" align="left">
							<input type="range" style="width: 400px;" class="custom-range"
								min="1" max="5" step="1" id="inputItemTaste2" name="taste_b"
								value="${idb.tasteB}">
						</div>
					</div>

					<div class="form-group row">
						<label for="FormControlTextarea" class="col-sm-3 col-form-label"
							align="right">説明</label>
						<div class="col-sm-5" align="left">
							<textarea class="form-control" style="width: 400px;"
								id="FormControlTextarea" rows="5" name="detail">${idb.detail}</textarea>
						</div>
					</div>
				</div>
			</div>


			<div class="form-group row" align="center" style="padding: 50px 0px">

				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;更新&emsp;&emsp;</button>
				</div>
			</div>

		</form>

		<div class="form-group row" align="right" style="padding-bottom: 50px">
			<div class="col-sm-3">
				<a type="submit"
					class="text-secondary border-bottom border-secondary" href="Item?item_id=${idb.id}">戻る</a>
			</div>
		</div>

	</div>

</body>
</html>