<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userDataDeleteConfirm</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>


<body>

	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- 本体 -->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>退会確認</h1>
		</div>

		<form action="UserDataDeleteResult" method="post">

			<input type="hidden" value="${udb.id}" name="id">

			<div class="col-sm-12 text-center">
				<h5>本当に退会しますか？</h5>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary"
						name="confirm_button" value="delete">&emsp;&ensp;はい&ensp;&emsp;</button>
					<button type="submit" class="btn btn-outline-secondary offset-md-2"
						name="confirm_button" value="cancel">&emsp;いいえ&emsp;</button>
				</div>
			</div>
		</form>

	</div>
</body>
</html>