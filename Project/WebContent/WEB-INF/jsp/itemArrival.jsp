<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>itemArrival</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>商品新規登録</h1>
		</div>

		<!-- 登録失敗 -->
		<c:if test="${validationMessage != null}">
			<h5 class="text-secondary text-center">${validationMessage}</h5>
			<br>
		</c:if>

		<form action="ItemArrival" method="post">

			<div class="form-group row">
				<label for="inputItemName" class="col-sm-4 col-form-label"
					align="right">商品名</label>
				<div class="col-sm-5" align="left">
					<input type="text" style="width: 400px;" class="form-control"
						name="name" value="${idb.name}" id="inputItemName">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputItemPrice" class="col-sm-4 col-form-label"
					align="right">単価</label>
				<div class="col-sm-5" align="left">
					<input type="text" style="width: 400px;" class="form-control"
						name="price" value="${idb.price}" id="inputItemPrice">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputItemOrigin" class="col-sm-4 col-form-label"
					align="right">原産地</label>
				<div class="col-sm-5" align="left">
					<input type="text" style="width: 400px;" class="form-control"
						name="origin" value="${idb.origin}" id="inputItemOrigin">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputItemTasteA" class="col-sm-4 col-form-label"
					align="right">酸味⇔苦味</label>
				<div class="col-sm-5" align="left">
					<input type="range" style="width: 400px;" class="custom-range"
						name="taste_a" value="${idb.tasteA}" min="1" max="5" step="1"
						id="inputItemTasteA">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputItemTasteB" class="col-sm-4 col-form-label"
					align="right">スッキリ⇔コク</label>
				<div class="col-sm-5" align="left">
					<input type="range" style="width: 400px;" class="custom-range"
						name="taste_b" value="${idb.tasteB}" min="1" max="5" step="1"
						id="inputItemTasteB">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputItemPic" class="col-sm-4 col-form-label"
					align="right">画像</label>
				<div class="col-sm-5" align="left">
					<input type="file" class="form-control-file" id="inputItemPic"
						name="file_name" value="${idb.fileName}">
				</div>
			</div>

			<div class="form-group row">
				<label for="TextAreaDetail" class="col-sm-4 col-form-label"
					align="right">説明</label>
				<div class="col-sm-5" align="left">
					<textarea class="form-control" style="width: 400px;" name="detail"
						value="${idb.detail}" id="TextAreaDetail" rows="5"></textarea>
				</div>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 0px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;登録&emsp;&emsp;</button>
				</div>
			</div>

			<div class="form-group row" align="right"
				style="padding-bottom: 50px">
				<div class="col-sm-3">
					<a type="submit"
						class="text-secondary border-bottom border-secondary" href="${referer}">戻る</a>
				</div>
			</div>

		</form>

	</div>
</body>
</html>