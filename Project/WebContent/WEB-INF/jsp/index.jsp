<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>index</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesome読み込み -->
<script src="https://kit.fontawesome.com/6c4d6d73a4.js"
	crossorigin="anonymous"></script>
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="main">
		<div class="container">

			<!-- メッセージ -->
			<c:if test="${succesMessage != null}">
				<br>
				<h5 class="text-secondary text-center">${succesMessage}</h5>
				<br>
			</c:if>

			<div class="top" style="padding: 50px 0">
				<img
					src="https://www.pakutaso.com/shared/img/thumb/coffee20160715235104_TP_V.jpg"
					class="img-fluid" alt="Responsive image">
				<h1>コーヒー豆のECサイト</h1>
			</div>

			<div class="row center">
				<h4 class="col-sm-2 offset-1" style="padding-bottom: 20px">
					<span class="material-icons"> star_border </span> おすすめ
				</h4>
			</div>

			<!--   おすすめ商品   -->
			<div class="card-deck col-sm-12" style="padding-bottom: 50px">
				<c:forEach var="item" items="${itemList}">
					<div class="card col-sm-4">
						<div align="center">
							<a href="Item?item_id=${item.id}"><img
								src="img/${item.fileName}"></a>
						</div>
						<div class="card-body">
							<h5 class="card-title">${item.name}</h5>
							<p class="card-text">${item.origin}</p>
							<p class="card-text">${item.formatPrice}円</p>
						</div>
					</div>

				</c:forEach>
			</div>


			<div class="row center">
				<h4 class="col-sm-2 offset-1" style="padding: 20px">
					<span class="material-icons"> search </span> 探す
				</h4>
			</div>

			<form action="ItemSearchResult" method="get">

				<div class="col-sm-8 offset-2">

					<div class="form-group" style="padding: 20px 0px">
						<label for="InputKeyWord">キーワード</label> <input type="text"
							name="search_word" class="form-control" id="InputKeyWord"
							placeholder="例）ブレンド">
					</div>

					<div class="form-group" style="padding: 20px 0px">
						<label for="InputOrigin">原産国</label> <input type="text"
							name="origin" class="form-control" id="InputOrigin"
							placeholder="例）ブラジル">
					</div>

					<div class="form-group" style="padding: 20px 0px">
						<label for="customRange1">予算</label>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" name="price_check" value="on"
								checked="checked" class="custom-control-input" id="price_check"
								onclick="connecttext('price_range',this.checked);"> <label
								class="custom-control-label" for="price_check">指定しない</label>
						</div>

						<h6>
							<input type="range" id="price_range" name="price"
								class="custom-range offset-1" style="width: 500px;" min="0"
								max="10000" step="100" value="10000"><label
								for="price_range">
								<div>
									&emsp;&emsp;<span id="value">10000</span>円まで
								</div>
							</label>
						</h6>

					</div>

					<div class="form-group" style="padding: 20px 0px">
						<label class="mt-3" for="customRange2">味</label>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" name="taste_check" value="on"
								checked="checked" class="custom-control-input" id="taste_check"
								onclick="connecttext('taste_range',this.checked);"> <label
								class="custom-control-label" for="taste_check">指定しない</label>
						</div>


						<h6>
							&ensp;&emsp;&emsp;酸味&emsp;&emsp; <input type="range"
								name="taste_a" class="custom-range" style="width: 500px;"
								min="1" max="5" step="1" id="taste_range">
							&emsp;&emsp;苦味&emsp;&emsp;
						</h6>

						<h6>
							&ensp;&emsp;スッキリ&emsp; <input type="range" name="taste_b"
								class="custom-range" style="width: 500px;" min="1" max="5"
								step="1" id="taste_range"> &emsp;&emsp;コク&emsp;&emsp;
						</h6>
					</div>

				</div>

				<div class="form-group row" align="center"
					style="padding: 50px 50px">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;&emsp;検索&emsp;&emsp;&emsp;</button>
					</div>
				</div>

			</form>

		</div>
	</div>



	<!-- javascript -->
	<script>
		// 現在の表示金額を表示
		var elem = document.getElementById('price_range');
		var target = document.getElementById('value');
		var rangeValue = function(elem, target) {
			return function(evt) {
				target.innerHTML = elem.value;
			}
		}
		elem.addEventListener('input', rangeValue(elem, target));

		// 金額にカンマ
		function comma(num) {
			return String(num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
		}

		/*
		// チェックボックスで入力を無効
		// 予算検索
		function connecttext(price_range, ischecked) {
			if (ischecked == true) {
				// チェックが入っていたら無効化
				document.getElementById(price_range).disabled = true;
			} else {
				// チェックが入っていなかったら有効化
				document.getElementById(price_range).disabled = false;
			}
		}

		// 味検索
		function connecttext(taste_range, ischecked) {
			if (ischecked == true) {
				// チェックが入っていたら無効化
				document.getElementById(taste_range).disabled = true;
			} else {
				// チェックが入っていなかったら有効化
				document.getElementById(taste_range).disabled = false;
			}
		}
		 */
	</script>



</body>
</html>