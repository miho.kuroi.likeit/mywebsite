<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>itemDelete</title>
<!-- bootstrap読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- CSS読み込み -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/baselayout/stylesheet.css">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>商品情報削除</h1>
		</div>

		<form action="ItemDelete" method="post">

			<input type="hidden" value="${idb.id}" name="item_id">

			<div class="form-group row">

				<div class="col-sm-5" align="right">
					<img src="img/${idb.fileName}" class="img-fluid"
						alt="Responsive image">
				</div>

				<div class="col-sm- offset-1" align="left">
					<h2>${idb.name}</h2>
					<h4>${idb.formatPrice}円</h4>
					<h4>${idb.origin}産</h4>
					<fieldset disabled>
						<h4>
							&emsp;酸味&emsp; <input type="range" style="width: 150px;"
								class="custom-range" min="1" max="5" step="1" id="inputTaste1"
								value="${idb.tasteA}" readonly> 苦味
						</h4>
						<h4>
							スッキリ <input type="range" style="width: 150px;"
								class="custom-range" min="1" max="5" step="1" id="inputTaste2"
								value="${idb.tasteB}" readonly> コク
						</h4>
					</fieldset>
					<p>${idb.detail}</p>

				</div>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<h5 style="padding-bottom: 20px">本当に削除しますか？</h5>
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;削除&emsp;&emsp;</button>
				</div>
			</div>

		</form>

		<div class="form-group row" align="right" style="padding-bottom: 50px">
			<div class="col-sm-3">
				<a type="submit"
					class="text-secondary border-bottom border-secondary" href="Item?item_id=${idb.id}">戻る</a>
			</div>
		</div>


	</div>

</body>
</html>