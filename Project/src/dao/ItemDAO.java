package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemDataBeans;

/**
 * 商品表示
 */
public class ItemDAO {

	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したい数
	 * @return <ItemDataBeans>
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setOrigin(rs.getString("origin"));
				item.setFileName(rs.getString("file_name"));
				item.setTasteA(rs.getInt("taste_a"));
				item.setTasteB(rs.getInt("taste_b"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品IDによる商品検索
	 * @param itemId
	 * @return ItemDataBeans
	 * @throws SQLException
	 */
	public static ItemDataBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setOrigin(rs.getString("origin"));
				item.setFileName(rs.getString("file_name"));
				item.setTasteA(rs.getInt("taste_a"));
				item.setTasteB(rs.getInt("taste_b"));
			}

			System.out.println("searching item by itemID has been completed");

			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品検索
	 * @param searchWord
	 * @param idb
	 * @param pageNum
	 * @param pageMaxItemCount
	 * @return itemList
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getItemsBySearch(String searchWord, ItemDataBeans idb, int pageNum,
			int pageMaxItemCount)
			throws SQLException {
		Connection con = null;

		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			// SELECT文を準備
			String sql;
			if (searchWord.equals("") && idb.getOrigin().equals("") && idb.getPrice() == 0
					&& idb.getTasteA() == 0 && idb.getTasteB() == 0) {
				// 全検索
				sql = "SELECT * FROM m_item ORDER BY id ASC LIMIT "
						+ startiItemNum + ", " + pageMaxItemCount;
				System.out.println("all search");
			} else {
				// 色々検索
				sql = "SELECT * FROM m_item";
				String conj = " WHERE";

				if (!searchWord.equals("")) {
					sql += conj + " name LIKE '%" + searchWord
							+ "%' OR detail LIKE '%" + searchWord + "%'";
					conj = " AND";
				}

				if (!idb.getOrigin().equals("")) {
					sql += conj + " origin LIKE '%" + idb.getOrigin() + "%'";
					conj = " AND";
				}

				if (idb.getPrice() != 0) {
					sql += conj + " price <= " + idb.getPrice();
					conj = " AND";
				}

				if (idb.getTasteA() != 0) {
					sql += conj + " taste_a = " + idb.getTasteA();
					conj = " AND";
				}

				if (idb.getTasteB() != 0) {
					sql += conj + " taste_a = " + idb.getTasteB();
				}

				sql += " ORDER BY id ASC LIMIT " + startiItemNum + ", " + pageMaxItemCount;

				System.out.println("choice:" + sql);
			}

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setOrigin(rs.getString("origin"));
				item.setFileName(rs.getString("file_name"));
				item.setTasteA(rs.getInt("taste_a"));
				item.setTasteB(rs.getInt("taste_b"));
				itemList.add(item);
			}
			System.out.println("get Items by searching has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品総数を取得
	 *
	 * @param searchWord
	 * @param idb
	 * @return coung
	 * @throws SQLException
	 */
	public static double getItemCount(String searchWord, ItemDataBeans idb) throws SQLException {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			// SELECT文を準備
			String sql;
			if (searchWord.equals("") && idb.getOrigin().equals("") && idb.getPrice() == 0
					&& idb.getTasteA() == 0 && idb.getTasteB() == 0) {
				// 全検索
				sql = "SELECT COUNT(*) AS CNT FROM m_item";
				System.out.println("all search number");
			} else {
				// 色々検索
				sql = "SELECT COUNT(*) AS CNT FROM m_item";
				String conj = " WHERE";

				if (!searchWord.equals("")) {
					sql += conj + " name LIKE '%" + searchWord
							+ "%' OR detail LIKE '%" + searchWord + "%'";
					conj = " AND";
				}

				if (!idb.getOrigin().equals("")) {
					sql += conj + " origin LIKE '%" + idb.getOrigin() + "%'";
					conj = " AND";
				}

				if (idb.getPrice() != 0) {
					sql += conj + " price <= " + idb.getPrice();
					conj = " AND";
				}

				if (idb.getTasteA() != 0) {
					sql += conj + " taste_a = " + idb.getTasteA();
					conj = " AND";
				}

				if (idb.getTasteB() != 0) {
					sql += conj + " taste_a = " + idb.getTasteB();
				}

				System.out.println("choice number:" + sql);
			}

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * データの挿入処理を行う
	 *
	 * @param item
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void insertItem(ItemDataBeans idb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO m_item(name, detail, price, origin, file_name, taste_a, taste_b) VALUES(?,?,?,?,?,?,?)");
			st.setString(1, idb.getName());
			st.setString(2, idb.getDetail());
			st.setInt(3, idb.getPrice());
			st.setString(4, idb.getOrigin());
			st.setString(5, idb.getFileName());
			st.setInt(6, idb.getTasteA());
			st.setInt(7, idb.getTasteB());

			st.executeUpdate();
			System.out.println(st);
			System.out.println("inserting item has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品情報の更新処理を行う。
	 *
	 * @param
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void updateItem(ItemDataBeans idb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			if (!idb.getFileName().equals("")) {
				// データベースへ接続
				con = DBManager.getConnection();
				st = con.prepareStatement(
						"UPDATE m_item SET name = ?, detail = ?, price = ?, origin = ?, file_name = ?, taste_a = ?, taste_b = ? WHERE id = ?");
				st.setString(1, idb.getName());
				st.setString(2, idb.getDetail());
				st.setInt(3, idb.getPrice());
				st.setString(4, idb.getOrigin());
				st.setString(5, idb.getFileName());
				st.setInt(6, idb.getTasteA());
				st.setInt(7, idb.getTasteB());
				st.setInt(8, idb.getId());
			} else {
				con = DBManager.getConnection();
				st = con.prepareStatement(
						"UPDATE m_item SET name = ?, detail = ?, price = ?, origin = ?, taste_a = ?, taste_b = ? WHERE id = ?");
				st.setString(1, idb.getName());
				st.setString(2, idb.getDetail());
				st.setInt(3, idb.getPrice());
				st.setString(4, idb.getOrigin());
				st.setInt(5, idb.getTasteA());
				st.setInt(6, idb.getTasteB());
				st.setInt(7, idb.getId());
			}
			st.executeUpdate();
			System.out.println(st);
			System.out.println("update has been completed");

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品情報の削除処理を行う。
	 *
	 * @param item
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void deleteItem(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			st = con.prepareStatement("DELETE FROM m_item WHERE id = ?");
			st.setInt(1, id);

			System.out.println(st);

			st.executeUpdate();

			st.close();

			System.out.println("delete has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
