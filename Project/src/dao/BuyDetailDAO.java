package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

/**
 * 購入情報の詳細
 */
public class BuyDetailDAO {

	/**
	 * 購入詳細登録処理
	 * @param bddb BuyDetailDataBeans
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy_detail(buy_id, item_id, amount, user_id) VALUES(?,?,?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.setInt(3, bddb.getAmount());
			st.setInt(4, bddb.getUserId());
			st.executeUpdate();
			System.out.println("inserting BuyDetail has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return {BuyDataDetailBeans}
	 * @throws SQLException
	 */
	public static ArrayList<BuyDetailDataBeans> getBuyDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_buy_detail WHERE buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDetailDataBeans> buyDetailList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setItemId(rs.getInt("item_id"));
				buyDetailList.add(bddb);
			}

			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return buyDetailList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * TODO:IDによる購入情報検索
	 * @param id
	 * @return {BuyDetailDataBeans}
	 * @throws SQLException
	 */
	public static ArrayList<BuyDetailDataBeans> getBuyDetailBeansListById(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT t_buy_detail.id,"
							+ " t_buy_detail.buy_id,"
							+ " t_buy_detail.item_id"
							+ " FROM t_buy_detail"
							+ " JOIN t_buy"
							+ " ON t_buy.id = t_buy_detail.buy_id"
							+ " WHERE t_buy.id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();
			System.out.println(st);

			ArrayList<BuyDetailDataBeans> BuyDetailDataList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setItemId(rs.getInt("item_id"));

				BuyDetailDataList.add(bddb);
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return null;
	}

	/**
	* 購入IDによる購入詳細情報検索
	* @param buyId
	* @return buyDetailItemList ArrayList<ItemDataBeans>
	*             購入詳細情報のデータを持つJavaBeansのリスト
	* @throws SQLException
	*/
	public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT m_item.id,"
							+ " m_item.name,"
							+ " m_item.price,"
							+ " t_buy_detail.amount,"
							+ " t_buy_detail.id"
							+ " FROM t_buy_detail"
							+ " JOIN m_item"
							+ " ON t_buy_detail.item_id = m_item.id"
							+ " WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("m_item.id"));
				idb.setName(rs.getString("m_item.name"));
				idb.setPrice(rs.getInt("m_item.price"));
				idb.setAmount(rs.getInt("t_buy_detail.amount"));
				idb.setDetailId(rs.getInt("t_buy_detail.id"));

				buyDetailItemList.add(idb);
			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");
			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	* IDによる購入詳細情報検索
	* @param id
	* @return buyDetailItemList ArrayList<ItemDataBeans>
	*             購入詳細情報のデータを持つJavaBeansのリスト
	* @throws SQLException
	*/
	public static ArrayList<ItemDataBeans> getItemDataBeansListById(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT m_item.id,"
							+ " m_item.name,"
							+ " m_item.price"
							+ " FROM t_buy_detail"
							+ " JOIN m_item"
							+ " ON t_buy_detail.item_id = m_item.id"
							+ " WHERE t_buy_detail.id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));

				buyDetailItemList.add(idb);
			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");
			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * detailIDによるコメント検索
	 * @param id
	 * @return comment
	 * @throws SQLException
	 */
	public static String getCommentById(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT comment FROM t_buy_detail WHERE id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();


			String comment = null;
			while (rs.next()) {
			comment = rs.getString("comment");
			}

			System.out.println("searching comment by ID has been completed");
			return comment;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * アイテムIDによるコメント検索
	 * @param id
	 * @return buyDetailList
	 * @throws SQLException
	 */
	public static ArrayList<BuyDetailDataBeans> getCommentByItemId(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_buy_detail WHERE item_id = ? AND comment IS NOT NULL");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			ArrayList<BuyDetailDataBeans> buyDetailList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setComment(rs.getString("comment"));
				buyDetailList.add(bddb);
			}

			System.out.println("searching comment by ID has been completed");
			return buyDetailList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	/**
	 * コメントを登録・更新する
	 * @param id, comment
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void updateComment(int id, String comment) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_buy_detail SET comment = ? WHERE id = ?");
			st.setString(1, comment);
			st.setInt(2, id);

			st.executeUpdate();
			System.out.println(st);
			System.out.println("inserting comment has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * コメントの削除処理を行う。
	 *
	 * @param id
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void deleteComment(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String sql = "UPDATE t_buy_detail SET comment = NULL WHERE id = ?";

			// ステートメント生成
			st = con.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			st.setInt(1, id);

			// 登録SQL実行
			System.out.println(sql);
			st.executeUpdate();

			st.close();

			System.out.println("delete has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
