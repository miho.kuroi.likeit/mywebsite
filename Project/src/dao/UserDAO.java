package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserDataBeans;
import mywebsite.Helper;

/**
 * ユーザー
 */
public class UserDAO {

	/**
	 * データの挿入処理を行う。現在時刻は挿入直前に生成
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_user(login_id, password, name, birth_date, address, create_date) VALUES(?,?,?,?,?,NOW())");
			st.setString(1, udb.getLoginId());
			st.setString(2, Helper.getSha256(udb.getPassword()));
			st.setString(3, udb.getName());
			st.setString(4, udb.getBirthDate());
			st.setString(5, udb.getAddress());

			st.executeUpdate();
			System.out.println(st);
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;

			while (rs.next()) {
				if (Helper.getSha256(password).equals(rs.getString("password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}

			System.out.println("searching userId by loginId has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザーIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */
	public static UserDataBeans getUserDataBeansByUserId(int userId) throws SQLException {
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT id, login_id, password, name, address, birth_date FROM t_user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			//SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setPassword(rs.getString("password"));
				udb.setName(rs.getString("name"));
				udb.setAddress(rs.getString("address"));
				udb.setBirthDate(rs.getString("birth_date"));
			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return udb;
	}

	/**
	 * ユーザー情報の更新処理を行う。
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void updateUser(UserDataBeans udb) throws SQLException {
		// 更新された情報をセットされたJavaBeansのリスト
		//UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE t_user SET"
					+ " name = '" + udb.getName()
					+ "', birth_date = '" + udb.getBirthDate()
					+ "', address = '" + udb.getAddress()
					+ "'";

			if (!udb.getPassword().equals("")) {
				sql += ", password = '" + Helper.getSha256(udb.getPassword()) + "'";
			}

			sql += " WHERE id =" + udb.getId();

			// SELECTを実行し、結果表を取得
			System.out.println(sql);
			Statement stmt = con.createStatement();

			stmt.executeUpdate(sql);
			System.out.println("update has been completed");

			stmt.close();

			st = con.prepareStatement(
					"SELECT login_id, password, name, birth_date, address FROM t_user WHERE id=" + udb.getId());
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setLoginId(rs.getString("login_id"));
				udb.setPassword(rs.getString("password"));
				udb.setName(rs.getString("name"));
				udb.setBirthDate(rs.getString("birth_date"));
				udb.setAddress(rs.getString("address"));
			}

			st.close();
			System.out.println("searching updated-UserDataBeans has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザー情報の削除処理を行う。
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void deleteUser(UserDataBeans udb) throws SQLException {
		// 更新された情報をセットされたJavaBeansのリスト
		//UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String sql = "DELETE FROM t_user WHERE id = ?";

			// ステートメント生成
			st = con.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			st.setInt(1, udb.getId());

			// 登録SQL実行
			System.out.println(sql);
			st.executeUpdate();

			st.close();

			System.out.println("delete has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * loginIdの重複チェック
	 *
	 * @param loginId
	 *            check対象のログインID
	 * @param userId
	 *            check対象から除外するuserID
	 * @return bool 重複している
	 * @throws SQLException
	 */
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	/**
	 * すべてのユーザー情報を取得する
	 *
	 * @param
	 * @return すべてのデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */
	public List<UserDataBeans> findAll() {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			System.out.println(sql);

			SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String birthDate = df.format(rs.getDate("birth_date"));
				String address = rs.getString("address");
				int id = rs.getInt("id");
				UserDataBeans user = new UserDataBeans(loginId, password, name, birthDate, address, id);

				userList.add(user);
			}

			stmt.close();
			System.out.println("getting user list is done");

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	* 検索したユーザ情報を取得
	* @param
	* @return 該当のデータを格納する
	* @throws SQLException
	*             呼び出し元にcatchさせるためスロー
	*/

	public List<UserDataBeans> findUser(String targetLoginId, String targetName, String birthDateFrom,
			String birthDateTo, String targetAddress) {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user";
			String conj = " WHERE";

			if (!targetLoginId.equals("")) {
				sql += conj + " login_id = '" + targetLoginId + "'";
				conj = " AND";
			}

			if (!targetName.equals("")) {
				sql += conj + " name LIKE '%" + targetName + "%'";
				conj = " AND";
			}

			if (!birthDateFrom.equals("") && !birthDateTo.equals("")) {
				sql += conj + " birth_date BETWEEN '" + birthDateFrom + "' AND '" + birthDateTo + "'";
				conj = " AND";
			}

			if (!targetAddress.equals("")) {
				sql += conj + " address LIKE '%" + targetAddress + "%'";
			}

			// SELECTを実行し、結果表を取得
			// 確認用
			System.out.println(sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String birthDate = df.format(rs.getDate("birth_date"));
				String address = rs.getString("address");
				int id = rs.getInt("id");

				UserDataBeans user = new UserDataBeans(loginId, password, name, birthDate, address, id);

				userList.add(user);
			}

			stmt.close();
			System.out.println("getting user is done");

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

}
