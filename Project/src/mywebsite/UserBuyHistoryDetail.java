package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * 購入履歴画面
 *
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション開始
		HttpSession session = request.getSession();

		try {
			// ログイン時に取得したユーザーIDをセッションから取得
			//int userId = (int) session.getAttribute("userId");

			// URLからGETパラメータとしてBuyIDを受け取る
			int id = Integer.parseInt(request.getParameter("id"));

			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserIdでユーザーを取得
			//UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDAO.getUserDataBeansByUserId(userId)
			//		: (UserDataBeans) EcHelper.cutSessionAttribute(session, "returnUDB");

			// buyIDから購入情報を取得
			ArrayList<BuyDataBeans> bdb = BuyDAO.getBuyDataBeansListById(id);

			// buyIDから購入情報を取得
			ArrayList<ItemDataBeans> idb = BuyDetailDAO.getItemDataBeansListByBuyId(id);

			//TODO:buyIDから
			ArrayList<BuyDetailDataBeans> bddb = BuyDetailDAO. getBuyDataBeansListByBuyId(id);

			//String buyId= request.getParameter("id");
			//System.out.println("buyIdFrom:" + buyId);
			//request.setAttribute("buyId", buyId);

			//request.setAttribute("udb", udb);
			request.setAttribute("bdb", bdb);
			request.setAttribute("idb", idb);
			request.setAttribute("bddb", bddb);

			request.getRequestDispatcher(Helper.USER_BUY_HISTORY_DETAIL_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}



	}
}
