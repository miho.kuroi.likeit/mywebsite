package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * ユーザー情報更新入力内容確認画面
 *
 */
@WebServlet("/UserDataUpdateConfirm")
public class UserDataUpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();

		try {
			//int inputId = (int) session.getAttribute("userId");
			int inputId = Integer.parseInt(request.getParameter("id"));
			String inputLoginId = request.getParameter("login_id");
			String inputPassword = request.getParameter("password");
			String inputConfirmPassword = request.getParameter("confirm_password");
			String inputUserName = request.getParameter("user_name");
			String inputUserBirthDate = request.getParameter("birth_date");
			String inputUserAddress = request.getParameter("user_address");

			UserDataBeans udb = new UserDataBeans();
			udb.setId(inputId);
			udb.setLoginId(inputLoginId);
			udb.setPassword(inputPassword);
			udb.setName(inputUserName);
			udb.setBirthDate(inputUserBirthDate);
			udb.setAddress(inputUserAddress);

			//エラーメッセージを格納する変数
			String validationMessage = "";

			// 入力されているパスワードが確認用と等しいかチェック
			if (!inputPassword.equals(inputConfirmPassword)) {
				validationMessage += "入力されているパスワードと確認用パスワードが違います<br>";
			}

			// 入力項目に1つでも未入力のものがあるかチェック
			if (udb.getLoginId().equals("") || udb.getName().equals("")
					|| udb.getBirthDate().equals("") || udb.getAddress().equals("")) {
				validationMessage += "未入力の項目があります<br>";
			}

			//バリデーションエラーメッセージがないなら確認画面へ
			if (validationMessage.length() == 0) {
				//確認画面へ
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(Helper.USER_DATA_UPDATE_CONFIRM_PAGE).forward(request, response);
			} else {
				//セッションにエラーメッセージを持たせてユーザー画面へ
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("UserData");
			}

		} catch (

		Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
