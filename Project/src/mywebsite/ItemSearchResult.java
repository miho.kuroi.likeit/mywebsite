package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 検索結果画面
 *
 *
 */
@WebServlet("/ItemSearchResult")
public class ItemSearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示する商品数
	final static int PAGE_MAX_ITEM_COUNT = 6;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			/* セッションに遷移元情報が入ってたら破棄する
			String referer = (String) session.getAttribute("referer");
			if (referer != null) {
				session.removeAttribute("referer");
			}
			*/

			/*
			String referer = request.getQueryString();
			System.out.println(referer);
			session.setAttribute("referer", referer);
			*/

			/*
			if (pageNum != 1) {
			// 遷移元URLを取得してセッションに格納
			String referer = (String) request.getHeader("REFERER");
			System.out.println(referer);
			session.setAttribute("referer", referer);
			}
			*/

			//戻るボタンでセッションから遷移元URLを取り出して遷移
			//String url = (String) session.getAttribute("referer");
			//System.out.println(url);
			/*
			if (url != null) {
				request.getRequestDispatcher(response.encodeURL(url).substring(29)).forward(request, response);
				return;
			}
			*/

			//System.out.println(request.getPathInfo());
			//System.out.println(request.getPathTranslated());

			/* 戻るボタン押し下げでアクセスしてきたときはセッションから入力情報を取得
			ItemDataBeans idb = session.getAttribute("idb") != null
					? (ItemDataBeans) Helper.cutSessionAttribute(session, "idb")
					: new ItemDataBeans();

			String searchWord = session.getAttribute("searchWord") != null
					? (String) Helper.cutSessionAttribute(session, "searchWord")
					: toString();

			if (idb == null && searchWord == null) {
			*/
			//検索欄に入力されたデータを取得
			String searchWord = request.getParameter("search_word");
			if (searchWord.equals("")) {
				searchWord = (String) session.getAttribute("search_word");
				if(searchWord == null) {
					searchWord = "";
				}
			}
			String inputOrigin = request.getParameter("origin");
			int inputPrice = Integer.parseInt(request.getParameter("price"));
			int inputTasteA = Integer.parseInt(request.getParameter("taste_a"));
			int inputTasteB = Integer.parseInt(request.getParameter("taste_b"));

			//確認用
			System.out.println("searchWord:" + searchWord);
			System.out.println("inputOrigin:" + inputOrigin);
			System.out.println("inputPrice:" + inputPrice);
			System.out.println("inputTasteA:" + inputTasteA);
			System.out.println("inputTasteB:" + inputTasteB);

			//ビーンズにデータ格納
			ItemDataBeans idb = new ItemDataBeans();

			idb.setOrigin(inputOrigin);

			//チェックしなければ予算を追加
			String priceCheck = request.getParameter("price_check");
			System.out.println("priceCheck:" + priceCheck);

			if (priceCheck == null) {
				idb.setPrice(inputPrice);
			} else {
				idb.setPrice(0);
			}

			//チェックしなければ味を追加
			String tasteCheck = request.getParameter("taste_check");
			System.out.println("tasteCheck:" + tasteCheck);

			if (tasteCheck == null) {
				idb.setTasteA(inputTasteA);
				idb.setTasteB(inputTasteB);
			} else {
				idb.setTasteA(0);
				idb.setTasteB(0);
			}

			//}

			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("search_word", searchWord);
			session.setAttribute("idb", idb);

			/*System.out.println(searchWord);
			System.out.println(idb.getOrigin());
			System.out.println(idb.getPrice());
			System.out.println(idb.getTasteA());
			System.out.println(idb.getTasteB());
			*/

			// 商品リストを取得 ページ表示分のみ
			ArrayList<ItemDataBeans> searchResultItemList = ItemDAO.getItemsBySearch(searchWord, idb, pageNum,
					PAGE_MAX_ITEM_COUNT);

			// 検索ワードに対しての総ページ数を取得
			double itemCount = ItemDAO.getItemCount(searchWord, idb);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			System.out.println("pageMax:" + pageMax);
			System.out.println("pageNum:" + pageNum);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("itemList", searchResultItemList);

			request.getRequestDispatcher(Helper.SEARCH_RESULT_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
