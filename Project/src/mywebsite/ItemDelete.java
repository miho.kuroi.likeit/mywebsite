package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 商品情報削除確認
 *
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション開始
		HttpSession session = request.getSession();

		try {
			// URLからGETパラメータとしてIDを受け取る
			//String targetId = request.getParameter("id");
			// アイテムIDをセッションから取得
			int id = Integer.parseInt(request.getParameter("item_id"));
			//戻るページ表示用
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			// 遷移元URLを取得してセッションに格納
			//String referer = (String) request.getHeader("REFERER");
			//session.setAttribute("referer", referer);
			//System.out.println(referer);

			// ユーザ一覧情報を取得
			//UserDaon userDao = new UserDaon();
			//User user = userDao.UserDetail(targetId);

			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外itemIdでアイテムを取得
			ItemDataBeans idb = session.getAttribute("returnIDB") == null ? ItemDAO.getItemByItemID(id)
					: (ItemDataBeans) Helper.cutSessionAttribute(session, "returnIDB");

			request.setAttribute("idb", idb);
			request.setAttribute("pageNum", pageNum);

			request.getRequestDispatcher(Helper.ITEM_DELETE_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		try {
			// ログイン時に取得したアイテムIDをセッションから取得
			//int id = (int) session.getAttribute("item_id");

			int id = Integer.parseInt(request.getParameter("item_id"));
			//戻るページ表示用
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			System.out.println(id);
			System.out.println(pageNum);

			//対象のアイテム情報を取得
			ItemDataBeans idb = ItemDAO.getItemByItemID(id);

			//request.setAttribute("idb", idb);

			//System.out.println("getid:" + idb.getId());

			ItemDAO.deleteItem(id);
			request.setAttribute("idb", idb);
			request.setAttribute("pageNum", pageNum);
			String succesMessage = "商品情報を削除しました";
			session.setAttribute("succesMessage", succesMessage);
			//request.getRequestDispatcher(Helper.TOP_PAGE).forward(request, response);
			response.sendRedirect("Index");
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}