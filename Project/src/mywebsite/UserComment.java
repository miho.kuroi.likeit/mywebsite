package mywebsite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDetailDAO;

/**
 *	コメント登録画面
 * TODO:なにもできてない
 */
@WebServlet("/UserComment")
public class UserComment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * コメント登録画面へ遷移
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			// URLからGETパラメータとしてDetailIDを受け取る
			int id = Integer.parseInt(request.getParameter("id"));

			// 遷移元URLを取得してセッションに格納
			String referer = (String) request.getHeader("REFERER");
			session.setAttribute("referer", referer);
			System.out.println(referer);

			// コメント取得
			String comment = BuyDetailDAO.getCommentById(id);

			// セッションにセット
			request.setAttribute("id", id);
			request.setAttribute("comment", comment);

			// コメント入力画面に遷移
			request.getRequestDispatcher(Helper.USER_COMMENT_PAGE).forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
