package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDetailDAO;

/**
 * コメント登録完了画面
 * TODO:何にもできてない
 *
 */
@WebServlet("/UserCommentResult")
public class UserCommentResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			// detailIdをURLから取得
			int id = Integer.parseInt(request.getParameter("id"));

			// コメントを入力欄から取得
			String comment = request.getParameter("comment");

			// コメント登録・更新実行
			BuyDetailDAO.updateComment(id, comment);

			// セッションに入れる
			request.setAttribute("comment", comment);

			//コメント完了画面に遷移
			request.getRequestDispatcher(Helper.USER_COMMENT_RESULT_PAGE).forward(request, response);

		} catch (

		Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}