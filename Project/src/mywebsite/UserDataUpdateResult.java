package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ユーザー情報更新完了画面
 *
 *
 */
@WebServlet("/UserDataUpdateResult")
public class UserDataUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");

		// セッション開始
		HttpSession session = request.getSession();

		try {
			//int inputId = (int) session.getAttribute("userId");
			int inputId = Integer.parseInt(request.getParameter("id"));
			String inputLoginId = request.getParameter("login_id");
			String inputPassword = request.getParameter("password");
			String inputUserName = request.getParameter("user_name");
			String inputBirthDate = request.getParameter("birth_date");
			String inputUserAddress = request.getParameter("user_address");

			UserDataBeans udb = new UserDataBeans();
			udb.setId(inputId);
			udb.setLoginId(inputLoginId);
			udb.setPassword(inputPassword);
			udb.setName(inputUserName);
			udb.setBirthDate(inputBirthDate);
			udb.setAddress(inputUserAddress);

			// 確定ボタンが押されたかを確認する変数
			String confirmed = request.getParameter("confirm_button");

			switch (confirmed) {
			// キャンセル→セッションに入力内容を保持してユーザー情報画面へ
			case "cancel":
				session.setAttribute("returnUDB", udb);
				response.sendRedirect("UserData");
				break;
			// 更新→更新処理
			case "update":
				UserDAO.updateUser(udb);
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(Helper.USER_DATA_UPDATE_RESULT_PAGE).forward(request, response);
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}