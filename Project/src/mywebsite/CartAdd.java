package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 商品追加画面
 *
 */
@WebServlet("/CartAdd")
public class CartAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			//選択された商品のIDを型変換し利用
			int id = Integer.parseInt(request.getParameter("item_id"));
			int amount = Integer.parseInt(request.getParameter("amount"));

			//確認
			System.out.println("item_id:" + id);
			System.out.println("amount:" + amount);

			//対象のアイテム情報を取得
			ItemDataBeans item = ItemDAO.getItemByItemID(id);


			item.setAmount(amount);
			System.out.println("item.amount:" + item.getAmount());

			//追加した商品を表示するためリクエストパラメーターにセット
			request.setAttribute("item", item);
			//request.setAttribute("amount", amount);

			//カートを取得
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			//セッションにカートがない場合カートを作成
			if (cart == null) {
				cart = new ArrayList<ItemDataBeans>();
			}
			//カートに商品を追加。
			cart.add(item);
			//カート情報更新
			session.setAttribute("cart", cart);
			//session.setAttribute("amount", amount);
			request.setAttribute("cartActionMessage", "商品を追加しました");
			request.getRequestDispatcher(Helper.CART_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
