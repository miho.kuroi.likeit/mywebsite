package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ユーザー情報削除完了画面
 *
 *
 */
@WebServlet("/UserDataDeleteResult")
public class UserDataDeleteResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		try {
			// ログイン時に取得したユーザーIDをセッションから取得
			//int userId = (int) session.getAttribute("userId");
			int userId = Integer.parseInt(request.getParameter("id"));

			// userIdでユーザーを取得
			UserDataBeans udb = UserDAO.getUserDataBeansByUserId(userId);

			request.setAttribute("udb", udb);

			// 確定ボタンが押されたかを確認する変数
			String confirmed = request.getParameter("confirm_button");

			switch (confirmed) {
			// 確定ボタンが押されていなかった場合はセッションに入力内容を保持してユーザー情報画面へ
			case "cancel":
				//session.setAttribute("returnUDB", udb);
				//session.setAttribute("referer", referer);
				session.setAttribute("returnUDB", udb);
				response.sendRedirect("UserData");
				break;
			// 削除処理
			case "delete":
				UserDAO.deleteUser(udb);
				//request.setAttribute("udb", udb);
				session.removeAttribute("udb");
				request.getRequestDispatcher(Helper.USER_DATA_DELETE_RESULT_PAGE).forward(request, response);
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}