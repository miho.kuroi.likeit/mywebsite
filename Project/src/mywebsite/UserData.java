package mywebsite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.UserDAO;

/**
 * ユーザー情報画面
 * TODO:履歴一覧表示
 *
 */
@WebServlet("/UserData")
public class UserData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション開始
		HttpSession session = request.getSession();

		try {
			//セッションからユーザーIDを取得
			int userId = (int) session.getAttribute("userId");

			//管理人の場合、URLからユーザーIDを取得・上書き
			if (userId == 1) {
				String id = request.getParameter("id");
				System.out.println(id);
				if (id != null) {
					userId = Integer.parseInt(id);
				}
			}

			// ユーザー情報の取得、更新確認画面から戻ってきた場合はセッションから、それ以外はユーザーIDから
			UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDAO.getUserDataBeansByUserId(userId)
					: (UserDataBeans) Helper.cutSessionAttribute(session, "returnUDB");

			// 購入情報を取得、ユーザーIDから
			ArrayList<BuyDataBeans> bdb = BuyDAO.getBuyDataBeansListByUserId(userId);
			Collections.reverse(bdb);

			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
			String validationMessage = (String) Helper.cutSessionAttribute(session, "validationMessage");

			request.setAttribute("validationMessage", validationMessage);
			request.setAttribute("udb", udb);
			request.setAttribute("bdb", bdb);

			request.getRequestDispatcher(Helper.USER_DATA_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
