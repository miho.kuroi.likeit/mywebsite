package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ユーザー一覧（管理用）
 * TODO:ユーザー詳細への遷移（新規登録機能ができてから）
 *
 */
@WebServlet("/UserList")
public class UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserList() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ユーザ一覧情報を取得
		UserDAO userDAO = new UserDAO();
		List<UserDataBeans> userList = userDAO.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// 確認用
		//System.out.println("登録データ数:" + userList.size());

		// ユーザ一覧のjspにフォワード
		request.getRequestDispatcher(Helper.USER_LIST_PAGE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/** 検索 **/
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String targetLoginId = request.getParameter("targetLoginId");
		String targetName = request.getParameter("targetName");
		String birthDateFrom = request.getParameter("birthDateFrom");
		String birthDateTo = request.getParameter("birthDateTo");
		String targetAddress = request.getParameter("targetAddress");

		UserDAO userDAO = new UserDAO();
		List<UserDataBeans> userList = userDAO.findUser(targetLoginId, targetName, birthDateFrom, birthDateTo, targetAddress);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		//一覧画面にフォワード
		request.getRequestDispatcher(Helper.USER_LIST_PAGE).forward(request, response);


	}

}