package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 商品を新規登録する
 */
@WebServlet("/ItemArrival")
public class ItemArrival extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * ユーザー登録画面へ遷移
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		int pageNum = Integer
				.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

		// 遷移元URLを取得してセッションに格納
		String referer = (String) request.getHeader("REFERER");
		session.setAttribute("referer", referer);
		System.out.println(referer);

		//入力内容に誤りがある、確認画面で戻るボタン押し下げでアクセスしてきたときはセッションから入力情報を取得
		ItemDataBeans idb = session.getAttribute("idb") != null
				? (ItemDataBeans) Helper.cutSessionAttribute(session, "idb")
				: new ItemDataBeans();
		String validationMessage = (String) Helper.cutSessionAttribute(session, "validationMessage");

		//確認用
		System.out.println(idb);

		request.setAttribute("idb", idb);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("validationMessage", validationMessage);

		request.getRequestDispatcher(Helper.ITEM_ARRIVAL_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			String inputItemName = request.getParameter("name");
			int inputItemPrice = Integer.parseInt(request.getParameter("price"));
			String inputItemOrigin = request.getParameter("origin");
			int inputItemTasteA = Integer.parseInt(request.getParameter("taste_a"));
			int inputItemTasteB = Integer.parseInt(request.getParameter("taste_b"));
			String inputItemPic = request.getParameter("file_name");
			String TextAreaDetail = request.getParameter("detail");

			ItemDataBeans idb = new ItemDataBeans();
			idb.setName(inputItemName);
			idb.setPrice(inputItemPrice);
			idb.setOrigin(inputItemOrigin);
			idb.setTasteA(inputItemTasteA);
			idb.setTasteB(inputItemTasteB);
			idb.setFileName(inputItemPic);
			idb.setDetail(TextAreaDetail);

			String validationMessage = "";

			// 入力項目に1つでも未入力のものがあるか
			if (idb.getName().equals("") || idb.getPrice() == 0 || idb.getOrigin().equals("")
					|| idb.getTasteA() == 0 || idb.getTasteB() == 0 || idb.getDetail().equals("")) {
				validationMessage += "未入力の項目があります";
			}

			// バリデーションエラーメッセージがないならトップ画面へ
			if (validationMessage.length() == 0) {
				ItemDAO.insertItem(idb);
				request.setAttribute("idb", idb);
				String succesMessage = "商品情報を登録しました";
				session.setAttribute("succesMessage", succesMessage);
				response.sendRedirect("Index");
			} else {
				session.setAttribute("idb", idb);
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("ItemArrival");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
