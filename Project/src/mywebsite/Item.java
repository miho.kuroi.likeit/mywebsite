package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDetailDAO;
import dao.ItemDAO;

/**
 * 商品詳細画面
 *
 */
@WebServlet("/Item")
public class Item extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//選択された商品のIDを型変換し利用
			int id = Integer.parseInt(request.getParameter("item_id"));

			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			/*
			//戻るページ表示用
			String searchWord = (String) session.getAttribute("search_word");
			String priceCheck = (String) session.getAttribute("price_check");
			String tasteCheck = (String) session.getAttribute("taste_check");
			ItemDataBeans searchIdb = (ItemDataBeans) session.getAttribute("idb");
			request.setAttribute("searchWord", searchWord);
			request.setAttribute("priceCheck", priceCheck);
			request.setAttribute("tasteCheck", tasteCheck);
			request.setAttribute("searchIdb", searchIdb);
			*/

			// 遷移元URLを取得してセッションに格納
			String referer = (String) request.getHeader("REFERER");
			System.out.println(referer);
			if (referer.contains("ItemSearchResult") || referer.contains("Index")) {
				session.setAttribute("referer", referer);
				System.out.println(referer);
			}

			//対象のアイテム情報を取得
			ItemDataBeans idb = ItemDAO.getItemByItemID(id);

			// 対象のアイテムのコメントを取得
			ArrayList<BuyDetailDataBeans> bddb = BuyDetailDAO.getCommentByItemId(id);

			//リクエストパラメーターにセット
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("idb", idb);
			request.setAttribute("bddb", bddb);

			request.getRequestDispatcher(Helper.ITEM_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
