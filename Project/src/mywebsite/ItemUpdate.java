package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 商品情報更新
 */
@WebServlet("/ItemUpdate")
public class ItemUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			int id = Integer.parseInt(request.getParameter("item_id"));
			//戻るページ表示用
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			//対象のアイテム情報を取得
			ItemDataBeans idb = ItemDAO.getItemByItemID(id);

			//リクエストパラメーターにセット
			request.setAttribute("idb", idb);
			request.setAttribute("pageNum", pageNum);

			request.getRequestDispatcher(Helper.ITEM_UPDATE_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			int inputId = Integer.parseInt(request.getParameter("item_id"));
			String inputidbName = request.getParameter("name");
			int inputidbPrice = Integer.parseInt(request.getParameter("price"));
			String inputidbOrigin = request.getParameter("origin");
			int inputidbTasteA = Integer.parseInt(request.getParameter("taste_a"));
			int inputidbTasteB = Integer.parseInt(request.getParameter("taste_b"));
			String inputidbPic = request.getParameter("file_name");
			String TextAreaDetail = request.getParameter("detail");

			System.out.println(inputId);
			System.out.println(inputidbName);
			System.out.println(inputidbPrice);
			System.out.println(inputidbOrigin);
			System.out.println(inputidbTasteA);
			System.out.println(inputidbTasteB);
			System.out.println(inputidbPic);
			System.out.println(TextAreaDetail);

			ItemDataBeans idb = new ItemDataBeans();
			idb.setId(inputId);
			idb.setName(inputidbName);
			idb.setPrice(inputidbPrice);
			idb.setOrigin(inputidbOrigin);
			idb.setTasteA(inputidbTasteA);
			idb.setTasteB(inputidbTasteB);
			idb.setFileName(inputidbPic);
			idb.setDetail(TextAreaDetail);

			String validationMessage = "";

			// 入力項目に1つでも未入力のものがあるか
			if (idb.getName().equals("") || idb.getPrice() == 0 || idb.getOrigin().equals("")
					|| idb.getTasteA() == 0 || idb.getTasteB() == 0 || idb.getDetail().equals("")) {
				validationMessage += "未入力の項目があります";
			}

			// バリデーションエラーメッセージがないならトップ画面へ
			if (validationMessage.length() == 0) {
				//成功
				ItemDAO.updateItem(idb);
				request.setAttribute("idb", idb);
				String succesMessage = "商品情報を更新しました";
				session.setAttribute("succesMessage", succesMessage);
				//request.getRequestDispatcher(Helper.TOP_PAGE).forward(request, response);
				response.sendRedirect("Index");
			} else {
				//失敗
				session.setAttribute("idb", idb);
				session.setAttribute("validationMessage", validationMessage);
				//int id = Integer.parseInt(request.getParameter("idb_id"));
				//リクエストパラメーターにセット
				//戻るページ表示用
				//int pageNum = Integer
				//		.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

				//対象のアイテム情報を取得
				//ItemDataBeans idb = ItemDAO.getidbByItemID(id);
				//request.setAttribute("idb", idb);
				//request.setAttribute("pageNum", pageNum);
				//request.setAttribute("pageNum", pageNum);
				response.sendRedirect("itemUpdate");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
