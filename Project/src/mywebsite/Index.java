package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * トップ画面
 * TODO:おすすめをランダムに表示
 * TODO:検索機能
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			//セッションにsuccesMessageが入っていたら破棄する
			String succesMessage = (String) session.getAttribute("succesMessage");
			if (succesMessage != null) {
				session.removeAttribute("succesMessage");
			}

			// セッションに遷移元情報が入ってたら破棄する
			String referer = (String) session.getAttribute("referer");
			if (referer != null) {
				session.removeAttribute("referer");
			}

			//商品情報を取得
			ArrayList<ItemDataBeans> itemList = ItemDAO.getRandItem(3);

			//リクエストスコープにセット
			request.setAttribute("itemList", itemList);

			//セッションに検索情報が入っていたら破棄する
			String searchWord = (String)session.getAttribute("search_word");
			if(searchWord != null) {
				session.removeAttribute("search_word");
			}
			ItemDataBeans idb = (ItemDataBeans) session.getAttribute("idb");
			if (idb != null) {
				session.removeAttribute("idb");
			}

			request.getRequestDispatcher(Helper.TOP_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
