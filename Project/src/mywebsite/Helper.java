package mywebsite;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * 定数保持、処理及び表示簡略化ヘルパークラス
 *
 */
public class Helper {
	// 購入
	static final String BUY_PAGE = "/WEB-INF/jsp/buy.jsp";
	// 購入確認
	static final String BUY_CONFIRM_PAGE = "/WEB-INF/jsp/buyConfirm.jsp";
	// 購入完了
	static final String BUY_RESULT_PAGE = "/WEB-INF/jsp/buyResult.jsp";
	// 買い物かごページ
	static final String CART_PAGE = "/WEB-INF/jsp/cart.jsp";
	// エラーページ
	static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
	// TOPページ
	static final String TOP_PAGE = "/WEB-INF/jsp/index.jsp";
	// 商品新規登録ページ
	static final String ITEM_ARRIVAL_PAGE = "/WEB-INF/jsp/itemArrival.jsp";
	// 商品情報削除ページ
	static final String ITEM_DELETE_PAGE = "/WEB-INF/jsp/itemDelete.jsp";
	// 商品情報詳細ページ
	static final String ITEM_PAGE = "/WEB-INF/jsp/itemDetail.jsp";
	// 検索結果
	static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/itemSearchResult.jsp";
	// 商品情報更新ページ
	static final String ITEM_UPDATE_PAGE = "/WEB-INF/jsp/itemUpdate.jsp";
	// ログイン
	static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
	// ログアウト
	static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";
	// ユーザー新規登録
	static final String REGIST_PAGE = "/WEB-INF/jsp/regist.jsp";
	// ユーザー新規登録入力内容確認
	static final String REGIST_CONFIRM_PAGE = "/WEB-INF/jsp/registConfirm.jsp";
	// ユーザー新規登録完了
	static final String REGIST_RESULT_PAGE = "/WEB-INF/jsp/registResult.jsp";
	// ユーザー購入履歴
	static final String USER_BUY_HISTORY_DETAIL_PAGE = "/WEB-INF/jsp/userBuyHistoryDetail.jsp";
	// コメント入力
	static final String USER_COMMENT_PAGE = "/WEB-INF/jsp/userComment.jsp";
	// コメント削除完了
	static final String USER_COMMENT_DELETE_RESULT_PAGE = "/WEB-INF/jsp/userCommentDeleteResult.jsp";
	// コメント登録完了
	static final String USER_COMMENT_RESULT_PAGE = "/WEB-INF/jsp/userCommentResult.jsp";
	// ユーザー情報
	static final String USER_DATA_PAGE = "/WEB-INF/jsp/userData.jsp";
	// ユーザー情報削除確認
	static final String USER_DATA_DELETE_COMFIRM_PAGE = "/WEB-INF/jsp/userDataDeleteConfirm.jsp";
	// ユーザー情報削除完了
	static final String USER_DATA_DELETE_RESULT_PAGE = "/WEB-INF/jsp/userDataDeleteResult.jsp";
	// ユーザー情報一覧
	static final String USER_LIST_PAGE = "/WEB-INF/jsp/userList.jsp";
	// ユーザー情報更新確認
	static final String USER_DATA_UPDATE_CONFIRM_PAGE = "/WEB-INF/jsp/userDataUpdateConfirm.jsp";
	// ユーザー情報更新完了
	static final String USER_DATA_UPDATE_RESULT_PAGE = "/WEB-INF/jsp/userDataUpdateResult.jsp";

	public static Helper getInstance() {
		return new Helper();
	}

	/**
	 * 商品の合計金額を算出する
	 *
	 * @param items
	 * @return total
	 */
	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total += item.getPrice() * item.getAmount();
		}
		return total;
	}

	/**
	 * ハッシュ関数
	 *
	 * @param target
	 * @return
	 */

	public static String getSha256(String target) {
		MessageDigest md = null;
		StringBuffer buf = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(target.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++) {
				buf.append(String.format("%02x", digest[i]));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	/**
	 * ログインIDのバリデーション
	 *
	 * @param inputLoginId
	 * @return
	 */
	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}

		return false;

	}

}
