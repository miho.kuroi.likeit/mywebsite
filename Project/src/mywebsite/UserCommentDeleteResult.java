package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDetailDAO;

/**
 * コメント削除完了画面
 * TODO:何にもできてない
 *
 */
@WebServlet("/UserCommentDeleteResult")
public class UserCommentDeleteResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		try {
			// detailIdをURLから取得
			int id = Integer.parseInt(request.getParameter("id"));

			// コメント取得
			String comment = BuyDetailDAO.getCommentById(id);

			// コメント登録・更新実行
			BuyDetailDAO.deleteComment(id);

			// セッションに入れる
			request.setAttribute("comment", comment);

			request.getRequestDispatcher(Helper.USER_COMMENT_DELETE_RESULT_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}