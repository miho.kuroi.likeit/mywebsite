package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ユーザー情報削除確認画面
 *
 */
@WebServlet("/UserDataDeleteConfirm")
public class UserDataDeleteConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション開始
		HttpSession session = request.getSession();

		try {
			// URLからGETパラメータとしてIDを受け取る
			//String targetId = request.getParameter("id");
			// ログイン時に取得したユーザーIDをセッションから取得
			//int userId = (int) session.getAttribute("userId");
			int userId = Integer.parseInt(request.getParameter("id"));

			// ユーザ一覧情報を取得
			//UserDaon userDao = new UserDaon();
			//User user = userDao.UserDetail(targetId);

			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserIdでユーザーを取得
			UserDataBeans udb = UserDAO.getUserDataBeansByUserId(userId);

			request.setAttribute("udb", udb);

			request.getRequestDispatcher(Helper.USER_DATA_DELETE_COMFIRM_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
