package beans;

import java.io.Serializable;

/**
 * アイテム
 */
public class ItemDataBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String origin;
	private String fileName;
	private int tasteA;
	private int tasteB;
	private int amount;
	private int detailId;

	//コンストラクタ
	public ItemDataBeans(int id, String name, String detail, int price, String origin, String fileName, int tasteA,
			int tasteB) {
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.origin = origin;
		this.fileName = fileName;
		this.tasteA = tasteA;
		this.tasteB = tasteB;
	}

	//カート用コンストラクタ
	public ItemDataBeans(int id, String name, String detail, int price, String origin, String fileName, int tasteA,
			int tasteB, int amount) {
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.origin = origin;
		this.fileName = fileName;
		this.tasteA = tasteA;
		this.tasteB = tasteB;
		this.amount = amount;
	}

	public ItemDataBeans() {
	}

	public int getId() {
		return id;
	}

	public void setId(int itemId) {
		this.id = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String itemName) {
		this.name = itemName;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int itemPrice) {
		this.price = itemPrice;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String filename) {
		this.fileName = filename;
	}

	public int getTasteA() {
		return tasteA;
	}

	public void setTasteA(int tasteA) {
		this.tasteA = tasteA;
	}

	public int getTasteB() {
		return tasteB;
	}

	public void setTasteB(int tasteB) {
		this.tasteB = tasteB;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}


	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}


	public String getFormatTotalPrice() {
		return String.format("%,d", this.price * this.amount);
	}

	public int getDetailId() {
		return detailId;
	}

	public void setDetailId(int detailId) {
		this.detailId = detailId;
	}


}
