package beans;

import java.io.Serializable;

/**
 * ユーザー
 */
public class UserDataBeans implements Serializable {
	private String loginId;
	private String password;
	private String name;
	private String birthDate;
	private String address;
	private int id;

	// コンストラクタ
	public UserDataBeans() {
		this.loginId = "";
		this.password = "";
		this.name = "";
		this.birthDate = "";
		this.address = "";
	}

	// コンストラクタ
	public UserDataBeans(String loginId, String password, String name, String birthDate, String address, int id) {
		this.loginId = loginId;
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
		this.address = address;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
