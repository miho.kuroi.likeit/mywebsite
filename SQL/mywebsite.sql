-- 「mywebsite」データベース作成、宣言
CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
USE mywebsite;


-- 「m_delivery_method」テーブル作成
CREATE TABLE m_delivery_method (
    id          SERIAL              PRIMARY KEY     AUTO_INCREMENT,
    name        VARCHAR(256),
    price       INT
);

-- 「t_user」テーブル作成
CREATE TABLE t_user (
    id          SERIAL              PRIMARY KEY     AUTO_INCREMENT,
    name        VARCHAR(255)        NOT NULL,
    address     VARCHAR(255)        NOT NULL,
    login_id    VARCHAR(255)        NOT NULL        UNIQUE,
    password    VARCHAR(255)        NOT NULL,
    create_date DATE                NOT NULL,
    birth_date  DATE                NOT NULL
);

-- 「m_item」テーブル作成
CREATE TABLE m_item (
    id          SERIAL              PRIMARY KEY     AUTO_INCREMENT,
    name        VARCHAR(256)        NOT NULL,
    detail      TEXT                NOT NULL,
    price       INT                 NOT NULL,
    origin      VARCHAR(256)        NOT NULL,
    file_name   VARCHAR(256)        NOT NULL,
    taste_a     INT                 NOT NULL,
    taste_b     INT                 NOT NULL
);

-- 「t_buy」テーブル作成
CREATE TABLE t_buy (
    id                  SERIAL      PRIMARY KEY     AUTO_INCREMENT,
    user_id             INT,
    total_price         INT,
    delivery_method_id  INT,
    create_date         DATETIME
);

-- 「t_buy_detail」テーブル作成
CREATE TABLE t_buy_detail (
    id          SERIAL              PRIMARY KEY     AUTO_INCREMENT,
    buy_id      INT,
    item_id     INT,
    amount      INT,
    user_id     INT,
    comment     TEXT
);



-- 管理者のデータを登録
INSERT INTO t_user(id, name, address, login_id, password, create_date, birth_date)
    VALUES (1, '管理者', '東京都', 'admin', 'password', NOW(), '2020-01-01');

-- 配送方法のデータを登録
INSERT INTO m_delivery_method(name, price) VALUES
    ('通常', 0), ('お急ぎ', 500), ('日時指定', 200)
;

-- 商品のデータを登録（サンプル）
INSERT INTO m_item(name, detail, price, origin, file_name, taste_a, taste_b) VALUES
    ('AGF ちょっと贅沢な珈琲店 レギュラーコーヒー スペシャルブレンド', 'ブラジル産最上級グレード豆♯2の特徴を活かした、濃く芳醇な香りがひきたつ上質なコクをお楽しみいただけます。', 637, 'ブラジル', 'https://images-na.ssl-images-amazon.com/images/I/71cGI8sZrGL._AC_SL1500_.jpg', 4, 5),
    ('AGF ちょっと贅沢な珈琲店 レギュラーコーヒー キリマンジャロブレンド', 'タンザニアキリマンジャロ豆の特徴を活かした、爽やかな香りとすっきりとした後味をお楽しみいただけます。', 637, 'タンザニア', 'https://images-na.ssl-images-amazon.com/images/I/716MQm45YRL._AC_SL1500_.jpg', 2, 5),
    ('AGF ちょっと贅沢な珈琲店 レギュラーコーヒー モカブレンド', 'エチオピアモカ豆の特徴を活かした、フルーティーな香りと甘くコク豊かな味わいをお楽しみいただけます。', 637, 'エチオピア', 'https://images-na.ssl-images-amazon.com/images/I/71g7r-E67pL._AC_SL1500_.jpg', 2, 3)
;

